package com.mystore1.pojos;

/**
 * POJO empleado para recibir los valores de una forma Web
 * @author Gabriel Gonzalez
 *
 */
public class FrmLogin {
	private String mail;
	private String password;
	
	public FrmLogin() {}
	
	/**
	 * Constructor completo
	 * @param mail
	 * @param password
	 */
	public FrmLogin(String mail, String password) {
		this.mail = mail;
		this.password = password;
	}



	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Login [mail=");
		stringBuilder.append(mail);
		stringBuilder.append(", password=");
		stringBuilder.append(password);
		stringBuilder.append("]");
		return stringBuilder.toString();
	}
}
