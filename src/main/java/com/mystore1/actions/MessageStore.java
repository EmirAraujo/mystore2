package com.mystore1.actions;

public class MessageStore extends BaseAction {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
    
    public MessageStore(){
        message = "Hello Struts User";
    }

    public String getMessage() {
        return message;
    }
}