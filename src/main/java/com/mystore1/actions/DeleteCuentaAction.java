package com.mystore1.actions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mystore1.actions.BaseAction;
import com.mystore1.hibernate.User;
import com.mystore1.hibernate.UserDAO;

public class DeleteCuentaAction extends BaseAction {

	/**
	 * 
	 */
	private static final Logger log = LogManager.getLogger(DeleteCuentaAction.class);

	private static final long serialVersionUID = 1L;

	
	public String deleteCuenta() {
		log.info("deleteCuenta()");
		if (getSession().get("mail") != null && !getSession().get("mail").equals("")) {
			User u = UserDAO.getUserbyMail(getSession().get("mail").toString());

			(new UserDAO()).deleteUser(u);
			log.info("CUENTA : "+u+" BORRADA ");
			return "success";
		}
		addFieldError("error2", "Usuario no logeado");

		return "no-autenticado";

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
