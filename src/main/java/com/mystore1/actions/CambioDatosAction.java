package com.mystore1.actions;

import com.mystore1.hibernate.User;
import com.mystore1.hibernate.UserDAO;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Acciones que un usuario con sesión iniciada puede hacer sobre ella:<br/>
 * <ul>
 * 	<li>Cambiar contraseña</li>
 *	<li>Cerrar sesión</li>
 * </ul>
 * @author Gabriel Gonzalez
 *
 */
public class CambioDatosAction extends BaseAction{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(CambioDatosAction.class);
	private User userCambioDatos;

	/**
	 * 
	 * @return
	 */
	public String cambiarDatos() {
		log.info("cambiarDatos()");
		log.info("Session: "+getSession());
		log.info("userFR: "+userCambioDatos);


		String mail = getSession().get("mail").toString();

		try {
			UserDAO usuarioDAO = new UserDAO();

			log.info("Verificando data anterior");
			User u = UserDAO.getUserbyMail(mail);

			if( u != null ) {
				log.info("Actualizando datos");
				u.setBancnumber(userCambioDatos.getBancnumber());
				u.setBancverify(userCambioDatos.getBancverify());
				u.setDirection(userCambioDatos.getDirection());
				u.setName(userCambioDatos.getName());
				usuarioDAO.updateUsuario(u);
				addActionMessage( "Datos cambiados exitosamente" );
				log.info("Datos cambiados exitosamente");
				log.info("Cerrando session");
				this.cerrarrSesion();
				return "success";
			}else {
				addActionError( "Datos incorrectos" );
				return "datos-no-cambiados";
			}
		}catch(Exception e) {
			log.error("Error desconocido", e);
			addActionError( e.getMessage() );
			return "error";
		}
	}

	public void validate(){
		log.info("validate()");

		log.info("Evaluando el POJO: " + userCambioDatos);

		if( userCambioDatos.getBancnumber() == null || userCambioDatos.getBancnumber().isEmpty() || userCambioDatos.getBancnumber().length()<8) {
			addFieldError("frmCambioPasswd.passwdAnterior", "Longitud minima 8 caracteres");
		}

		if( userCambioDatos.getBancverify() == null || userCambioDatos.getBancverify().isEmpty() || userCambioDatos.getBancverify().length()<3) {
			addFieldError("passwordConfirn", "Longitud minima 3 caracteres");
		}
		if( userCambioDatos.getDirection() == null || userCambioDatos.getDirection().isEmpty() || userCambioDatos.getDirection().length()<10) {
			addFieldError("userCambioDatos.direction", "Poner direccion completa");
		}
	}

	public User getUserCambioDatos() {
		return userCambioDatos;
	}

	public void setUserCambioDatos(User userCambioDatos) {
		this.userCambioDatos = userCambioDatos;
	}


}
