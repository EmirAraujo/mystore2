package com.mystore1.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.mystore1.hibernate.HibernateSessionFactory;
import com.mystore1.hibernate.Product;
import com.mystore1.hibernate.ProductDAO;
import com.mystore1.hibernate.Sales;
import com.mystore1.hibernate.SalesDAO;
import com.mystore1.hibernate.User;
import com.mystore1.hibernate.UserDAO;
import com.mystore1.pojos.FrmCambioPassword;
import com.mystore1.pojos.FrmLogin;
import com.mystore1.pojos.FrmUsuario;

public class MostrarFormasAction extends BaseAction {
	private static final Logger log = LogManager.getLogger(MostrarFormasAction.class);

	private FrmLogin frmLogin;
	private FrmUsuario usuario;
	private FrmCambioPassword frmCambioPasswd;
	private List<Product> listP;
	private User userCambioDatos;
	private List<Sales> listS;
	private double total;

	private String flag;

	/**
	 * Acción que preparará la forma web para el inicio de sesión
	 * del usuario. 
	 * 
	 * Esta función que será ejecutada por la acción showLoginForm definida en el
	 * archivo de configuración struts.xml.
	 * @return
	 */
	public String showLoginForm() {
		log.info("showLoginForm()");

		log.info("Instanciando POJO para la forma login");
		frmLogin = new FrmLogin();
		log.info("Instanciando ListP para productos");
		listP = ProductDAO.getProducts();
		log.info("LISTP: "+listP);
		return "success";
	}

	public String showRegisterForm() {
		log.info("showRegisterForm()");

		usuario = new FrmUsuario();

		return "success";
	}


	public String showChangePasswordForm(){
		log.info("showChangePasswordForm()");
		frmCambioPasswd = new FrmCambioPassword();
		return "success";
	}
	public String showCambioDatos() {
		log.info("showCambioDatos()");
		if (getSession().get("mail") != null && !getSession().get("mail").equals("")) {
			userCambioDatos = UserDAO.getUserbyMail(getSession().get("mail").toString());
			if(userCambioDatos != null)
				return "success";
		}
		return "no-autenticado";

	}
	public String showSales() {
		log.info("showSales()");
		if (getSession().get("mail") != null && !getSession().get("mail").equals("")) {
			listS = SalesDAO.getSalesByMail(getSession().get("mail").toString());
			if(listS != null && listS.size()>0) {
				this.calcPrice();
				return "success";	
			}
		}
		return "no-autenticado";

	}
	private double calcPrice() {
		total = 0;
		for(Sales ss : listS)
			total += ss.getPrice();
		return total;
	}
	public String showHome(){
		return "success";
	}

	public String showCont() {
		return "success";
	}
	/**
	 * @return the frmLogin
	 */
	public FrmLogin getFrmLogin() {
		return frmLogin;
	}

	/**
	 * @param frmLogin the frmLogin to set
	 */
	public void setFrmLogin(FrmLogin frmLogin) {
		this.frmLogin = frmLogin;
	}

	/**
	 * @return the usuario
	 */
	public FrmUsuario getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(FrmUsuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the flag
	 */
	public String getFlag() {
		return flag;
	}

	/**
	 * @param flag the flag to set
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}

	/**
	 * @return the frmCambioPasswd
	 */
	public FrmCambioPassword getFrmCambioPasswd() {
		return frmCambioPasswd;
	}

	/**
	 * @param frmCambioPasswd the frmCambioPasswd to set
	 */
	public void setFrmCambioPasswd(FrmCambioPassword frmCambioPasswd) {
		this.frmCambioPasswd = frmCambioPasswd;
	}

	public List<Product> getListP() {
		listP = ProductDAO.getProducts();
		return listP;
	}

	public void setListP(List<Product> lispP) {
		this.listP = lispP;
	}

	public User getUserCambioDatos() {
		return userCambioDatos;
	}

	public void setUserCambioDatos(User userCambioDatos) {
		this.userCambioDatos = userCambioDatos;
	}

	public List<Sales> getListS() {
		return listS;
	}

	public void setListS(List<Sales> listS) {
		this.listS = listS;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}




}
