package com.mystore1.actions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mystore1.hibernate.Product;
import com.mystore1.hibernate.ProductDAO;
import com.mystore1.hibernate.Sales;
import com.mystore1.hibernate.SalesDAO;
import com.mystore1.hibernate.User;
import com.mystore1.hibernate.UserDAO;

public class cartAction extends BaseAction{

	/**
	 * 
	 */
	private static final Logger log = LogManager.getLogger(cartAction.class);

	private static final long serialVersionUID = 1L;
	private String buy ;
	private String del ;
	private List<Product> cart;
	private double totalC;


	public String updateCart() {
		log.info("updateCart()");
		log.info(""+buy);
		if (getSession().get("mail") != null) {
			String tmp = (String) getSession().get("cart");
			if(tmp == null )
				getSession().put("cart",buy);

			if( tmp != null && buy != null) {
				getSession().put("cart",tmp+","+ buy);
			}

			log.info(getSession());
			return "success";
		}
		return "no-autenticado";
	}
	public String deleteFromCart() {
		log.info("deleteFromCart()");
		log.info(""+del);
		if (getSession().get("mail") != null) {
			String tmp = (String) getSession().get("cart");
			if(tmp == null )
				getSession().put("cart","");
			boolean first = true;
			String cart2 = "";
			if( tmp != null && del != null && !del.equals("")) {
				for(String a : tmp.split(",")) {
					if(first && a.equals(del))
						first = false;
					else
						cart2 += a+",";
				}
				getSession().put("cart",cart2);
			}
			log.info(getSession());
			return "success";
		}
		return "no-autenticado";
	}
	public String showCart(){
		Object o= getSession().get("mail");
		String mail;
		log.info("showCart()"+o);
		if(o == null) {
			addActionError("usuario no logueado");
			return "no-autenticado";
		}
		mail = o.toString();
		User u = UserDAO.getUserbyMail(mail);
		if(u != null && !u.getMail().equals("")) {
			log.info("---- building cart ----");
			if (getSession().get("mail") != null) {
				String tmp = (String) getSession().get("cart");
				if(tmp != null && !tmp.equals("")) {
					int[] nums = Arrays.stream(tmp.split(",")).mapToInt(Integer::parseInt).toArray();  
					List<Integer> numsL = new ArrayList<Integer>();
					for (Integer a : nums)	
						numsL.add(a);
					log.info(""+nums+""+numsL);

					cart =  new ArrayList<Product>();
					for(Product p : ProductDAO.getProducts())
						if( numsL.contains( (int)p.getIdproduct()))
							cart.add(p);
					log.info(""+cart);

				}
				log.info(getSession());
				calcPrice();
				return "success";
			}
		}
		return "no-autenticado";
	}
	public String buyCart() {
		Object o= getSession().get("mail");
		if(o == null)
			return "no-autenticado";
		String mail = o.toString();
		log.info("buyCart()"+o);

		Sales s = new Sales();
		User u = UserDAO.getUserbyMail(mail);
		log.info("---- UserDAO : "+u);
		if(u != null && !u.getMail().equals("")) {
			if(!checkDataBanc(u)) {
				return "datos-incorrectos";
			}
			String tmp = getSession().get("cart").toString();
			int[] nums = Arrays.stream(tmp.split(",")).mapToInt(Integer::parseInt).toArray();  
			List<Integer> numsL = new ArrayList<Integer>();
			for (Integer a : nums)	
				numsL.add(a);
			String namesP="";
			double price = 0;
			for(Product p : ProductDAO.getProducts())
				if( numsL.contains( (int)p.getIdproduct())) {
					if(p.getAmount()>0) {
						p.setAmount(p.getAmount()-1);
						(new ProductDAO()).updateProduct(p);
						log.info("Inventario actualizado "+p);
						namesP+=p.getName()+" , ";
						price+=p.getPrice();
					}
				}

			s.setUser(u.getMail());
			s.setProds(namesP);
			s.setPrice(price);
			s.setStatus(Sales.STATUS.VALIDADO.name());
			s.setDate(LocalDateTime.now().toString());
			log.info("Venta generada: "+s);

			(new SalesDAO()).saveSale(s);
			log.info("---- Gracias por su compra ----");
			getSession().put("cart","");

			return "success";
		}
		log.info("---- Usuario no encontrado ----");
		addActionError("Usuario no encontrado");
		return "no-autenticado";

	}

	private boolean checkDataBanc(User u) {
		if(u==null || u.getBancnumber()==null || u.getBancverify()==null || u.getDirection()==null)
			return false;
		if(u.getBancnumber().length()<8 || u.getBancverify().length()<3 || u.getDirection().length() < 10) {
			addActionError("Datos bancarios o direccion incorrecta");
			return false;
		}
		return true;
	}
	private double calcPrice() {
		totalC = 0;
		if(cart == null)
			cart = new ArrayList<Product>();
		for(Product ss : cart)
			totalC += ss.getPrice();
		return totalC;
	}
	public String getBuy() {
		return buy;
	}

	public void setBuy(String buy) {
		this.buy = buy;
	}
	public List<Product> getCart() {
		return cart;
	}

	public void setCart(List<Product> cart) {
		this.cart = cart;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public String getDel() {
		return del;
	}
	public void setDel(String del) {
		this.del = del;
	}
	public double getTotalC() {
		return totalC;
	}
	public void setTotalC(double totalC) {
		this.totalC = totalC;
	}
}
