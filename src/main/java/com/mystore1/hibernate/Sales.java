package com.mystore1.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales")
public class Sales implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int salesid;
	private String user;
	private String status;
	private String date;
	private String prods;
	private double price;
	public enum STATUS{VALIDADO,EN_CAMINO};
	@Id
	@Column( name = "salesid", unique = true, nullable = false, length = 45 )
	public int getSalesid() {
		return salesid;
	}

	public void setSalesid(int salesid) {
		this.salesid = salesid;
	}
	@Column ( name = "user", nullable = false, length = 45 )
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	@Column ( name = "status", nullable = false, length = 45 )
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@Column ( name = "date", nullable = false, length = 45 )
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	@Column ( name = "prods", nullable = false, length = 45 )
	public String getProds() {
		return prods;
	}

	public void setProds(String prods) {
		this.prods = prods;
	}
	@Column ( name = "price", nullable = false, length = 45 )
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Sales [salesid=" + salesid + ", user=" + user + ", status=" + status + ", date=" + date + ", prods="
				+ prods + ", price=" + price + "]";
	}
}
