package com.mystore1.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class Product implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idproduct;
	private String name;
	private String description;
	private String path;
	private double price;      
	private String brand;    
	private long amount;    
	@Id
	@Column( name = "idproduct", unique = true, nullable = false, length = 45 )
	public int getIdproduct() {
		return idproduct;
	}

	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}
	@Column ( name = "name", nullable = false, length = 45 )
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column ( name = "description", nullable = false, length = 45 )
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column ( name = "path", nullable = false, length = 45 )
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	@Column ( name = "price", nullable = false, length = 45 )
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	@Column ( name = "brand", nullable = false, length = 45 )
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	@Column ( name = "amount", nullable = false, length = 45 )
	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "Products [name=" + name + ", description=" + description + ", path=" + path + ", price=" + price
				+ ", brand=" + brand + ", amount=" + amount + ", idproduct=" + idproduct + "]";
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
