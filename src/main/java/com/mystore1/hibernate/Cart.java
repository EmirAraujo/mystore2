package com.mystore1.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cart")
public class Cart implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int prod_id;
	private int amount;
	private float price;
	private int salesid;
	private String status;
	
	@Id
	@Column( name = "mail", unique = true, nullable = false, length = 45 )
	public int getProd_id() {
		return prod_id;
	}
	public void setProd_id(int prod_id) {
		this.prod_id = prod_id;
	}
	@Column ( name = "amount", nullable = false, length = 45 )
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Column ( name = "price", nullable = false, length = 45 )
	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	@Column ( name = "salesid", nullable = false, length = 45 )
	public int getSalesid() {
		return salesid;
	}

	public void setSalesid(int salesid) {
		this.salesid = salesid;
	}
	@Column ( name = "status", nullable = false, length = 45 )
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "cart [prod_id=" + prod_id + ", amount=" + amount + ", price=" + price + ", salesid=" + salesid
				+ ", status=" + status + "]";
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
