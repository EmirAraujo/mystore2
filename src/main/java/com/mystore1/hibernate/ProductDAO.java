package com.mystore1.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;


public class ProductDAO extends BaseHibernateDAO {

	public static List<Product> getAllProducts(){
		return (List<Product>)(List<?>)findAll(Product.class);
	}
	public static List<Product> getProducts(){
		return (List<Product>)(List<?>)findAll(Product.class);
	}
	public static List<Product> FindProds(int[] a){
		Criteria criteria = getSession().createCriteria(Product.class);
		for(Integer a1 : a) {
			criteria.add( Expression.or(Restrictions.eq("idproduct", a1)) );
		}
		System.out.println(""+criteria.list());
		return (List<Product>) criteria.list();
		}
	public void updateProduct(Product product) {
		super.saveOrUpdate(product);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print(ProductDAO.getProducts());
	}

}
