package com.mystore1.hibernate;

import java.util.Iterator;
import java.util.List;

import javax.management.Query;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;

//Import log4j classes.
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


@Entity
@Table(name = "users")
public class User implements java.io.Serializable{

	/**
	 * 
	 */
    private static final Logger log = LogManager.getLogger(User.class);
	
	private static final long serialVersionUID = 1L;
	private String mail="";
	private String password="";
	private String name="";
	private String direction="";
	private String bancnumber="";
	private String bancverify="";
	
	@Column ( name = "name", nullable = false, length = 45 )
	public String getName() {
		log.debug("name : "+name);
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Column ( name = "direction", nullable = false, length = 45 )
	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
	@Column ( name = "bancnumber", nullable = false, length = 45 )
	public String getBancnumber() {
		return bancnumber;
	}

	public void setBancnumber(String bancnumber) {
		this.bancnumber = bancnumber;
	}
	@Column ( name = "bancverify", nullable = false, length = 45 )
	public String getBancverify() {
		return bancverify;
	}

	public void setBancverify(String bancverify) {
		this.bancverify = bancverify;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Id
	@Column( name = "mail", unique = true, nullable = false, length = 45 )
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	@Column ( name = "password", nullable = false, length = 45 )
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [mail=" + mail + ", password=" + password + ", name=" + name + ", direction=" + direction
				+ ", bancnumber=" + bancnumber + ", bancverify=" + bancverify + "]";
	}
	
	//@PersistenceContext(unitName = "Persistencia")
	//public static EntityManager manager;
	
	public static void main(String[] args) {
		Configuration configuration = new Configuration();
		StandardServiceRegistry standardRegistry ;
		SessionFactory sessionFactory;
	    Session session ;
	    try {
			//configuration.configure("/home/mac/git/mystore1/src/resources/hibernate.cfg.xml");

	    	standardRegistry = 
	    		       new StandardServiceRegistryBuilder().configure().build();
	    			Metadata metaData = 
	    		        new MetadataSources(standardRegistry).getMetadataBuilder().build();
	    			sessionFactory = metaData.getSessionFactoryBuilder().build();
	    			session = sessionFactory.openSession();
	    			
	    			//session = sessionFactory.getCurrentSession();
		} catch (Exception e) {
			System.err.println("Error en el manejo de la sesión con hibernate");
			//logger.error("Error en el manejo de la sesión con hibernate", e);
			throw new RuntimeException(e);
		}
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    CriteriaQuery<User> criteria = builder.createQuery(User.class);
	    Root<User> root = criteria.from( User.class );
	    criteria.select( root );
	    List<User> persons = session.createQuery( criteria ).getResultList();

	   
	    //session.createNamedQuery("select * from tienda.users");
	    System.out.println(persons);
	    
	    //EntityManager manager;
	    //EntityManagerFactory emf;
	    //emf = Persistence.createEntityManagerFactory();
	    //manager = emf.createEntityManager();
	    //List<User> users1 =  manager.createQuery("From users").getResultList();
	    //System.out.println(users1.size()+"  "+users1);
	    
	    
	}

}
