package com.mystore1.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;


public class SalesDAO extends BaseHibernateDAO {

	public static List<Sales> getAllSales(){
		return (List<Sales>)(List<?>)findAll(Sales.class);
	}
	public static List<Sales> getSales(){
		return (List<Sales>)(List<?>)findAll(Sales.class);
	}
	public static List<Sales> getSalesByMail(String mail) {
		Criteria criteria = getSession().createCriteria(Sales.class)
			.add(Restrictions.eq("user", mail) );
		System.out.println(""+criteria.list());
		return (List<Sales>) criteria.list();
	}
	public void saveSale(Sales sales) {
		super.save(sales);
	}
	
	public void updateSales(Sales sales) {
		super.saveOrUpdate(sales);
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(SalesDAO.getSalesByMail("samm@gmail.com"));
		System.out.print(SalesDAO.getSales());
		Sales s = new Sales();
		s.setUser("holi");
		s.setDate("date");
		s.setStatus("vendido");
		s.setProds("1,2,3,4");
		(new SalesDAO()).save(s);
	}

}
