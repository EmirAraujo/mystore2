package com.mystore1.hibernate;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.mystore1.pojos.FrmLogin;

public class UserDAO extends BaseHibernateDAO{

	public static User login(FrmLogin login) {
		Criteria criteria = getSession().createCriteria(User.class)
				.add( Restrictions.eq("mail", login.getMail()) )
				.add( Restrictions.eq("password", login.getPassword()) );
		return (User) criteria.uniqueResult();
	}
	public static User getUserbyMail(String mail) {
		Criteria criteria = getSession().createCriteria(User.class)
				.add( Restrictions.eq("mail", mail) );
		return (User) criteria.uniqueResult();
	}

	public void saveUsuario(User usuario) {
		super.save(usuario);
	}

	public void updateUsuario(User usuario) {
		super.saveOrUpdate(usuario);
	}
	public void deleteUser(User u) {
		super.deleteEnt(u);
	}	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		User u = UserDAO.login(new FrmLogin("alan@gmail.com","Password123"));
		System.out.println(u);
		(new UserDAO()).deleteEnt(u);
	}

}
