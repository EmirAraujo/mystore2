package com.mystore1.hibernate;


import java.util.List;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

//import org.apache.logging.log4j.core.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
//import org.hibernate.service.ServiceRegistry;
//Import log4j classes.
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import com.mystore1.hibernate.User;

/**
 * Clase para administrar las sesiones (conexiones) a la bases de datos
 * @author Gabriel Gonzalez y Emir Araujo
 *
 */
@SuppressWarnings("deprecation")
public class HibernateSessionFactory {
    private static final Logger logger = LogManager.getLogger(HibernateSessionFactory.class);
	
	private static final ThreadLocal<Session> threadLocal = new ThreadLocal<Session>();
	private static Configuration configuration = new Configuration();
	private static StandardServiceRegistry standardRegistry ;
	private static SessionFactory sessionFactory;
	//private static Session session ;
    
    static {
    	try {
	    	standardRegistry = new StandardServiceRegistryBuilder().configure().build();
	    	Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
	    	sessionFactory = metaData.getSessionFactoryBuilder().build();
		} catch (Exception e) {
			System.err.println("Error en el manejo de la sesión con hibernate");
			logger.error("Error en el manejo de la sesión con hibernate", e);
			throw new RuntimeException(e);
		}
    }
    
    /**
     * Constructor por defecto
     */
    private HibernateSessionFactory() {}
	
	/**
     * Regresa el Thread Local con la instancia de sesión. La inicialización es de forma Lazy
     * para <code>SessionFactory</code> si es que se necesita.
     *
     *  @return Session
     *  @throws HibernateException
     */
    public static Session getSession(){
    	logger.debug("getSession()");
        Session session = (Session) threadLocal.get();

		if (session == null || !session.isOpen()) {
			logger.debug("Sesión nula o cerrada");
			if (sessionFactory == null) {
				rebuildSessionFactory();
			}
			logger.debug("Reconstruyendo la sesión");
			session = (sessionFactory != null) ? sessionFactory.openSession()
					: null;
			threadLocal.set(session);
		}
        return session;
    }

	/**
     *  Reconstruye la sesión
	 * @throws DatabaseOperationException 
     *
     */
	public static void rebuildSessionFactory(){
		try {
	    	standardRegistry = new StandardServiceRegistryBuilder().configure().build();
	    	Metadata metaData = new MetadataSources(standardRegistry).getMetadataBuilder().build();
	    	sessionFactory = metaData.getSessionFactoryBuilder().build();
		} catch (Exception e) {
			System.err.println("Error en la reconstrucción de la sesión");
			logger.error("Error en la reconstrucción de la sesión", e);
			throw new RuntimeException(e);
		}
	}

	/**
     *  Cierre del thread con la sesión actual de hibernate
     *
     *  @throws HibernateException
     */
    public static void closeSession() throws HibernateException {
        Session session = (Session) threadLocal.get();
        threadLocal.set(null);

        if (session != null) {
            session.close();
        }
    }

	/**
     *  return session factory
     *
     */
	public static org.hibernate.SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	/**
     *  return hibernate configuration
     *
     */
	public static Configuration getConfiguration() {
		return configuration;
	}
	public static void main(String[] args) {
		
	    Session session  = HibernateSessionFactory.getSession();
	   
	    
	    
	    CriteriaBuilder builder = session.getCriteriaBuilder();
	    CriteriaQuery<User> criteria = builder.createQuery(User.class);
	    Root<User> root = criteria.from(User.class);
	    criteria.select( root );
	    List<User> persons = session.createQuery( criteria ).getResultList();

	   
	    //session.createNamedQuery("select * from tienda.users");
	    System.out.println(persons);
	    System.out.println("////////////////////////////////////////////");
	    System.out.println(HibernateSessionFactory.getSession().getCriteriaBuilder().createQuery(User.class).from( User.class ));
	    
	    //EntityManager manager;
	    //EntityManagerFactory emf;
	    //emf = Persistence.createEntityManagerFactory();
	    //manager = emf.createEntityManager();
	    //List<User> users1 =  manager.createQuery("From users").getResultList();
	    //System.out.println(users1.size()+"  "+users1);
	    
	    
	}
}

