<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="card card-container">
	<h3>Carrito de compras</h3>
	<s:form action="buy">
		<s:if test="hasFieldErrors()">
			<div class="alert alert-warning fade in">
				<a href="#" class="close" data-dismiss="alert">×</a> <strong><s:fielderror /></strong>
			</div>
		</s:if>
		<button type="submit" class="btn btn-lg btn-primary btn-signin">Comprar</button>
	</s:form>
	
	<br>
	
	<table class = "tabel table-hover">
		<thead>
		<tr>
			<th>Marca</th>
			<th>Producto</th>
			<th>Precio</th>
			<th>Descripcion</th>
			<th>Eliminar</th>
		
		</tr>
		</thead>
		<s:iterator value="cart" status="name">
			<tr>
				<td><s:property value="brand" /></td>
				<td><s:property value="name" /></td>
				<td><s:property value="price" /></td>
				<td><s:property value="description" /></td>
				<s:form action="deleteFromCart" class="form-signin">
						<td><input type="submit" name="del"
						value="<s:property value='idproduct'/>" /></td>
				</s:form>

			</tr>
		</s:iterator>

	</table>
	<br>
	<p>Total: </p>
	<s:property value="totalC" />

</div>
<!--/.card card-container-->
