<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="card card-container">
	<h3>Historial</h3>
	<table class = "tabel table-hover">
		<thead>
		<tr>
			<th>Fecha de Compra</th>
			<th>Productos</th>
			<th>Total</th>
		</tr>
		</thead>
		<s:iterator value="listS" status="name">
			<tr>
				<td><s:property value="date" /></td>
				<td><s:property value="prods" /></td>
				<td><s:property value="price" /></td>
			</tr>
		</s:iterator>

	</table>
</div>
<!--/.card card-container-->
