<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="card card-container">
	<s:if test="hasActionErrors()">
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert">×</a> <strong><s:actionerror /></strong>
		</div>
	</s:if>
	<h3>Cambio de Datos</h3>
	<table class="tabel table-hover">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Direccion</th>
				<th>Numero de Cuenta</th>
				<th>Verificador</th>
			</tr>
		</thead>

		<s:form action="cdatos" namespace="/actions/pub" method="POST"
			class="form-signin">
			<span id="reauth-email" class="reauth-email"></span>
			<s:fielderror />
			<tr>
				<td><s:textfield name="userCambioDatos.name"
						class="form-control" placeholder="name"></s:textfield></td>
				<td><s:textfield name="userCambioDatos.direction"
						class="form-control" placeholder="direction"></s:textfield></td>

				<td><s:textfield name="userCambioDatos.bancnumber"
						class="form-control" placeholder="bancnumber"></s:textfield></td>

				<td><s:textfield name="userCambioDatos.bancverify"
						class="form-control" placeholder="bancverify"></s:textfield></td>
			</tr>
			<br>

			<button class="btn btn-lg btn-primary btn-block btn-signin"
				type="submit">Cambiar datos</button>

			<br>

		</s:form>
	</table>



	<a class="btn btn-lg btn-danger btn-block btn-signin" type="submit"
		href="<s:url action="deleteCuenta" namespace="/actions/pub"/>">Borrar
		cuenta</a>
</div>
<!-- /card-container -->