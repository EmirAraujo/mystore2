<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- Inicio login.jsp -->
<div class="card card-container">

	<%-- Manejo de mensajes --%>
	<s:set var="bandera" value="flag" />
	<%-- Verificación de la existencia de algun error generado en el controlador y
				 registrado a través de la función addActionError 
			--%>
	<s:if test="hasActionErrors()">
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert">×</a> <strong><s:actionerror /></strong>
		</div>
	</s:if>
	<!-- Inicio forma -->
	<s:form action="login" method="POST" class="form-signin">
		<span id="reauth-email" class="reauth-email"></span>
		<s:textfield name="frmLogin.mail" label="Correo" class="form-control"
			placeholder="usuario@dominio.com" autofocus="true" />
		<s:password name="frmLogin.password" label="Contraseña"
			class="form-control" placeholder="Contraseña" />
		<s:submit value="Ingresar"
			class="btn btn-lg btn-block btn-primary btn-signin" />
	</s:form>
	<!-- Fin forma -->
		<p>
		<a href='<s:url action="showRegisterForm" />' class="forgot-password">Aun
			no eres usuario?</a>
	</p>

	<h2>Agrega a tu carrito los siguientes productos:</h2>
	<table class = "tabel table-hover">
		<tr>
			<thead>
			<th>Marca</th>
			<th>Producto</th>
			<th>Precio</th>
			<th>Descripcion</th>
			<th>Cantidad</th>
			<th>Comprar</th>
			</thead>
		</tr>
		<s:form action="updateCart" class="form-signin">
		<s:iterator value="listP" status="name">
			<tr>
				<td><s:property value="brand" /></td>
				<td><s:property value="name" /></td>
				<td><s:property value="price" /></td>
				<td><s:property value="description" /></td>
				<td><s:property value="amount" /></td>
			    <td><input type="submit" name="buy" value="<s:property value='idproduct'/>" /></td>
			</tr>
		</s:iterator>
		</s:form>
	</table>
	
	<br>

</div>
<!-- /card-container -->
<!-- fin login.jsp -->