<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<tiles:insertAttribute name="styles" />
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<style>
table {
	border-collapse: collapse;
	width: 100%;
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #dddddd;
}

.inicio {
	background-color: red;
}
</style>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<!-- Static navbar -->
		<nav class="navbar navbar-default">
		<div class="container-fluid">

	    
		<div class="jumbotron text-center">
  		  	<h1>Mi Miscelanea Online</h1>
  			<p>Sin salir de tu casa</p> 
  	
  			<h2>Bienvenido ${sessionScope.name}</h2>
		  </div>
		
		
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a
						href="<s:url action="showLoginForm" namespace="/actions/pub"/>">Inicio</a></li>
					<li><a
						href="<s:url action="showCart" namespace="/actions/pub"/>">Carrito</a></li>
					<li><a
						href="<s:url action="showSales" namespace="/actions/pub"/>">Historial</a></li>
					<li><a
						href="<s:url action="showCambioPasswdForm" namespace="/actions/pub"/>">Cambio
							Contraseña</a></li>
					<li><a
						href="<s:url action="showCambioDatos" namespace="/actions/pub"/>">Cambio
							de Datos</a></li>
					<li><a
						href="<s:url action="logout" namespace="/actions/pub"/>">Cerrar
							Sesión</a></li>
					<li><a href="<s:url action="cont" namespace="/actions/pub"/>">Contacto</a></li>

				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		<!--/.container-fluid --> </nav>
		<!-- Contenido de la página -->
		<tiles:insertAttribute name="body" />
		<!-- Fin contenido de la página -->
	</div>
	<!-- /container -->
</body>
</html>